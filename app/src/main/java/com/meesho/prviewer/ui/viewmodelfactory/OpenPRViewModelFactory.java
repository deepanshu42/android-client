package com.meesho.prviewer.ui.viewmodelfactory;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.meesho.prviewer.data.GithubDataRepository;
import com.meesho.prviewer.ui.viewmodel.OpenPRViewModel;

public class OpenPRViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(OpenPRViewModel.class)) {
            return (T) new OpenPRViewModel(new GithubDataRepository());
        }
        throw new IllegalArgumentException("Wrong class");
    }
}
