package com.meesho.prviewer.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.meesho.prviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.owner_name)
    EditText ownerName;

    @BindView(R.id.repo_name)
    EditText repoName;

    @BindView(R.id.search)
    Button searchButton;

    public static final String EXTRA_GITHUB_OWNER_NAME = "owner_name";
    public static final String EXTRA_GITHUB_REPO_NAME = "repo_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();

        ownerName.setVisibility(View.VISIBLE);
        repoName.setVisibility(View.VISIBLE);
        searchButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        searchButton.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search:
                String githubOwnerName = ownerName.getText().toString();
                String githubRepoName = repoName.getText().toString();
                Intent intent = new Intent(this, OpenPRViewerActivity.class);
                intent.putExtra(EXTRA_GITHUB_OWNER_NAME, githubOwnerName);
                intent.putExtra(EXTRA_GITHUB_REPO_NAME, githubRepoName);
                startActivity(intent);
                break;
        }
    }
}
