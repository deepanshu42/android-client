package com.meesho.prviewer.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.meesho.prviewer.R;
import com.meesho.prviewer.data.model.PRItem;
import com.meesho.prviewer.ui.adapter.OpenPRAdapter;
import com.meesho.prviewer.ui.viewmodel.OpenPRViewModel;
import com.meesho.prviewer.ui.viewmodelfactory.OpenPRViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.meesho.prviewer.ui.activity.SearchActivity.EXTRA_GITHUB_OWNER_NAME;
import static com.meesho.prviewer.ui.activity.SearchActivity.EXTRA_GITHUB_REPO_NAME;


public class OpenPRViewerActivity extends AppCompatActivity {

    @BindView(R.id.search_header)
    TextView searchHeader;

    @BindView(R.id.pr_list)
    RecyclerView prListRv;

    private String searchHeaderString;

    private List<PRItem> openPRsList = new ArrayList<>();

    private OpenPRAdapter openPRAdapter;

    private RecyclerView.LayoutManager layoutManager;

    private OpenPRViewModel openPRViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_viewer);
        ButterKnife.bind(this);

        String ownerName = getIntent().getExtras().getString(EXTRA_GITHUB_OWNER_NAME);
        String repoName = getIntent().getExtras().getString(EXTRA_GITHUB_REPO_NAME);
        searchHeaderString = getResources().getString(R.string.search_header, ownerName, repoName);

        openPRViewModel = ViewModelProviders.of(this, new OpenPRViewModelFactory()).get(OpenPRViewModel.class);
        openPRViewModel.getResponseLiveData().observe(this, new Observer<List<PRItem>>() {
            @Override
            public void onChanged(@Nullable List<PRItem> openPRs) {
                if(openPRs.size() == 0) {
                    openPRViewModel.stopLoading();
                }
                openPRsList.addAll(openPRs);
                openPRAdapter.setOpenPRs(openPRsList);
                openPRAdapter.notifyDataSetChanged();
            }
        });

        layoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        prListRv.setLayoutManager(layoutManager);

        openPRAdapter = new OpenPRAdapter(this, openPRsList);
        prListRv.setAdapter(openPRAdapter);

        openPRViewModel.init(ownerName, repoName);
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchHeader.setText(searchHeaderString);
        searchHeader.setVisibility(View.VISIBLE);
        prListRv.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prListRv.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                if(lastPosition == openPRAdapter.getItemCount() - 1) {
                    openPRViewModel.loadNextPage();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
