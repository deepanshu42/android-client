package com.meesho.prviewer.ui.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.meesho.prviewer.data.GithubDataRepository;
import com.meesho.prviewer.data.model.PRItem;

import java.util.List;

public class OpenPRViewModel extends ViewModel {
    private MutableLiveData<List<PRItem>> responseLiveData;

    private GithubDataRepository githubDataRepository;

    private String owner;

    private String repo;

    private boolean loadNext = true;

    public OpenPRViewModel(GithubDataRepository githubDataRepository) {
        this.githubDataRepository = githubDataRepository;
        this.responseLiveData = new MutableLiveData<>();
    }

    public void init(String owner, String repo) {
        this.owner = owner;
        this.repo = repo;
        githubDataRepository.getOpenPRList(responseLiveData, owner, repo);
    }

    public void loadNextPage() {
        if(loadNext) {
            githubDataRepository.getOpenPRList(responseLiveData, owner, repo);
        }
    }

    public LiveData<List<PRItem>> getResponseLiveData() {
        return responseLiveData;
    }

    public void stopLoading() {
        loadNext = false;
    }
}
