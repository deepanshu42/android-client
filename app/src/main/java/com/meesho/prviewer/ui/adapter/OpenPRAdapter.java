package com.meesho.prviewer.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meesho.prviewer.R;
import com.meesho.prviewer.data.model.PRItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpenPRAdapter extends RecyclerView.Adapter<OpenPRAdapter.PRViewHolder> {

    private Context context;

    private List<PRItem> openPRsList;

    public OpenPRAdapter(Context context, List<PRItem> openPRsList) {
        this.context = context;
        this.openPRsList = openPRsList;
    }

    public void setOpenPRs(List<PRItem> openPRsList) {
        this.openPRsList = openPRsList;
    }

    @NonNull
    @Override
    public PRViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LinearLayout openPRview = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_prlist, viewGroup, false);

        return new PRViewHolder(openPRview);
    }

    @Override
    public void onBindViewHolder(@NonNull PRViewHolder viewHolder, int position) {
        PRItem item = openPRsList.get(position);

        viewHolder.number.setText(item.getNumber());
        viewHolder.title.setText(item.getTitle());
        viewHolder.author.setText(item.getAuthor().getUserName());
    }

    @Override
    public int getItemCount() {
        return openPRsList.size();
    }

    static class PRViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.number)
        TextView number;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.author)
        TextView author;

        PRViewHolder(@NonNull View openPRview) {
            super(openPRview);
            ButterKnife.bind(this, openPRview);
        }
    }
}
