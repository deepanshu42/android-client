package com.meesho.prviewer.data.remote;

import com.meesho.prviewer.data.model.PRItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubApiService {
    @GET("repos/{owner}/{repo}/pulls")
    Call<List<PRItem>> getOpenPRList(@Path("owner") String ownerName, @Path("repo") String repoName, @Query("page") int page, @Query("per_page") int perPageCount);
}
