package com.meesho.prviewer.data.remote;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.meesho.prviewer.data.model.PRItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteDataSource {

    private static final String GITHUB_BASE_URL = "https://api.github.com/";

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(GITHUB_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public void getOpenPRList(final MutableLiveData<List<PRItem>> liveData, String owner, String repo, int page, int pageCount) {

        GithubApiService githubApiService = retrofit.create(GithubApiService.class);

        Call<List<PRItem>> openPRResponseCall = githubApiService.getOpenPRList(owner, repo, page, pageCount);
        openPRResponseCall.enqueue(new Callback<List<PRItem>>() {
            @Override
            public void onResponse(Call<List<PRItem>> call, Response<List<PRItem>> response) {
                Log.d("RemoteDataSource", response.toString());
                if(response.isSuccessful()) {
                    List<PRItem> openPRsList = response.body();
                    liveData.postValue(filterPRList(openPRsList));
                }
            }

            @Override
            public void onFailure(Call<List<PRItem>> call, Throwable t) {
                Log.d("RemoteDataSource", t.getLocalizedMessage());
            }
        });
    }

    private List<PRItem> filterPRList(List<PRItem> openPRsList) {
        List<PRItem> filteredPRList = new ArrayList<>();
        for(PRItem item : openPRsList) {
            if(item.getState().equals("open")) {
                filteredPRList.add(item);
            }
        }
        return filteredPRList;
    }
}
