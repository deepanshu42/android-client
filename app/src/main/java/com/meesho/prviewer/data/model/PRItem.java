package com.meesho.prviewer.data.model;

import com.google.gson.annotations.SerializedName;

public class PRItem {

    @SerializedName("number")
    private String number;

    @SerializedName("title")
    private String title;

    @SerializedName("user")
    private Author author;

    @SerializedName("state")
    private String state;

    public PRItem(String number, String title, Author author, String state) {
        this.number = number;
        this.title = title;
        this.author = author;
        this.state = state;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
