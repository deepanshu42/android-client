package com.meesho.prviewer.data.local;

import java.util.HashMap;
import java.util.Map;

public class LocalDataSource {
    private Map<String, Integer> pageMap;

    public LocalDataSource() {
        this.pageMap = new HashMap<>();
    }

    public int getPage(String ownerName, String repoName) {
        String key = ownerName + "/" + repoName;
        if(pageMap.containsKey(key)) {
            int page = pageMap.get(key);
            pageMap.put(key, page + 1);
            return page + 1;
        } else {
            pageMap.clear();
            pageMap.put(key, 1);
            return 1;
        }
    }

    public int getPageSize() {
        return 100;
    }
}
