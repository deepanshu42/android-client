package com.meesho.prviewer.data;

import android.arch.lifecycle.MutableLiveData;

import com.meesho.prviewer.data.local.LocalDataSource;
import com.meesho.prviewer.data.model.PRItem;
import com.meesho.prviewer.data.remote.RemoteDataSource;

import java.util.List;

public class GithubDataRepository {
    private RemoteDataSource remoteDataSource;

    private LocalDataSource localDataSource;

    public GithubDataRepository() {
        this.remoteDataSource = new RemoteDataSource();
        this.localDataSource = new LocalDataSource();
    }

    public void getOpenPRList(MutableLiveData<List<PRItem>> photosLiveData, String owner, String repo) {
        remoteDataSource.getOpenPRList(photosLiveData, owner, repo, localDataSource.getPage(owner, repo), localDataSource.getPageSize());
    }
}
